<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Role extends Model
{
    /**
     * Get the users that belong to the role
     */

    protected $fillable = ['name'];

    public function users() {
        return $this->belongsToMany(User::class);
    }
}

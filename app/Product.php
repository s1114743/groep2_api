<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
   
    protected $fillable = ['name', 'price', 'description'];

    public function users() {
        return $this->belongsToMany(User::class);
    }

    public function orders() {
        return $this->belongsToMany(Order::class);
    }
}

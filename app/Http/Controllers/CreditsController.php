<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Exception;

class CreditsController extends Controller
{
    public function incrementCreditsToAccount(Request $request, $user) {
        try {
            $creditsInput = $request->credits;
            $creditsOld = User::where('name', '=', $user)->first()->credits;
            $totalCredits = $creditsOld + $creditsInput;

            User::where('name', $user)->update([
                'credits' => $totalCredits,
            ]);

            return response()->json([
                "Current credits in database " => $creditsOld,
                "Credits to be given " => $creditsInput,
                "New total credits " => $totalCredits,
            ]);

            $user->save();
        }

        catch(Exception $e) {
            return $e;
        }
    }

    public function decrementCreditsFromAccount(Request $request, $user) {
        try {
            $creditsInput = $request->credits;
            $creditsOld = User::where('name', '=', $user)->first()->credits;
            $totalCredits = $creditsOld - $creditsInput;

            User::where('name', $user)->update([
                'credits' => $totalCredits,
            ]);

            return response()->json([
                "Current credits in database " => $creditsOld,
                "Credits taken off " => $creditsInput,
                "New total credits " => $totalCredits,
            ]);

            $user->save();
        }

        catch(Exception $e) {
            return $e;
        }
    }
}

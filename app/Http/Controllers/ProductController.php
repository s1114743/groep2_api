<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Product;
use DB;

class ProductController extends Controller
{
    public function create(Request $request) {
        
        //request data valideren 
        $request->validate([
            'name' => 'required|string',
            'price' => 'required|numeric',
            'description' => 'required|string'
        ]);  

        $product = new Product([
            'name' => $request->name,
            'price' => $request->price,
            'description' => $request->description,
                // 'image' => $request->image
        ]);   

        //     //nieuw product opslaan in de database
        $product->save();

        //     //bericht terug sturen naar de front-end met de melding dat een nieuw product is toegevoegd
        return response()->json([
                'message' => 'Successfully added product'
            ], 201);
    }

    public function index(){
        

        return response()->json([
            Product::All()
        ], 201);
    }
}
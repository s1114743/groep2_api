<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Order;
use App\Product;
use DB;

class OrderController extends Controller
{
    public function create(Request $request) {

        //request data valideren 
        $request->validate([
            'product_id' => 'required|array',
            'user_name' => 'required|string'
        ]);    
        
        //user_name uit request opslaan in variabele
        $user_name = $request->user_name; 

        //de naam van de user opvragen en opslaan in variabele
        $user = User::get('name')->where('name', '=', $user_name)->first();

        //de role van de user opvragen en opslaan in variabele
        $user_role = DB::table('users')->select('role')->where('name', '=', $user_name)->get();

        // checken of de user die de post request verstuurt wel voorkomt in de database
        if ($user->name == $user_name && $user_role[0]->role == 'company') {

            //nieuwe order aanmaken
            $order = new Order([]);   
        
            //nieuwe order opslaan in de database
            $order->save();
        
            //user_id opvragen van de user die de post request heeft gedaan
            $user_id = User::select('id')->where('name', '=', $user_name)->first();

            //koppeling maken met user_id in de koppeltabel "order_user"
            $user_id->orders()->attach($user_id);
    
            //het aantal plekken van de product_id array binnen de request optellen en opslaan in variabele
            $steps = count($request->product_id);
    
            //de product_id's uit de request tellen en opslaan in variabele
            $request_product_id = $request->product_id;

            for ($i=0; $i < $steps; $i++) { 

                //product_id op plaats [$i] pakken uit de request en opslaan in variabele
                $product_id = Product::get('id')->where('id', '=', $request_product_id[$i])->first();
                
                //product_id met bijbehorende user_id toevoegen aan de koppeltabel "product_user"
                $product_id->orders()->attach($user_id);
            }       
            
            //bericht terug sturen naar de front-end met de melding dat een nieuwe order is toegevoegd
            return response()->json([
                'message' => 'Successfully created order'
            ], 201);
        }

        //bericht terug sturen naar de front-end met de melding dat heet maken van een nieuw product niet mogelijk is
        return response()->json([
            // 'message' => 'Authentication failed'
            'message' => $product_list
        ], 201);
        
    }
}
